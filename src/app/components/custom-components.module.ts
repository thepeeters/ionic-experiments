import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { LiveTimeComponent } from './livetime/livetime.component';
import { PlaceholderComponent } from './placeholder/placeholder.component';
import { ShowOrNotComponent } from './show-or-not/show-or-not.component';
import { VersionComponent } from './version/version.component';
import { VjPlayerComponent } from './vjs-player/vj-player.component';

 
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule,
    ReactiveFormsModule,
  ],
  declarations: [
    LiveTimeComponent,
    PlaceholderComponent,
    ShowOrNotComponent,
    VersionComponent,
    VjPlayerComponent,
    /* Other componet*/
  ],
  exports: [
    LiveTimeComponent,
    PlaceholderComponent,
    ShowOrNotComponent,
    VersionComponent,
    VjPlayerComponent

    /* Other componet*/
  ],
})
export class CustomComponentsModule {}
