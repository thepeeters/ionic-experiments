import { Component, OnInit } from '@angular/core';
import { CapacitorStorageService } from '../../services/capacitor-storage.service';

@Component({
  selector: 'app-storage',
  templateUrl: './storage.page.html',
  styleUrls: ['./storage.page.scss'],
})
export class StoragePage implements OnInit {

  table = []
  newKey: string;
  newValue: any;
  constructor(private storage: CapacitorStorageService) { }

  ngOnInit() {
  
  }

  async updateView(){
    this.table=[]
    let keysArray = await this.storage.keys();
    keysArray.forEach( async (key:string) => {
      let k = key;
      let v = await this.storage.get(key);
      let t = v.constructor.name
      this.table.push({key:k, value:v,type:t})
    })
  
  }
  async add_elements(){
    await this.storage.set('name','Max')
    await this.storage.set('MyBoolean',true)
    await this.storage.set('Lorem','ipsum')
    await this.storage.set('MyNumber',3)
    await this.updateView()
  }

  async remove_element(){
    await this.storage.remove('MyBoolean')
    await this.updateView()
  }
  async remove_all(){
    await this.storage.clear()
    await this.updateView()
  }
  
  

}
