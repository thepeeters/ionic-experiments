import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FullScreenBackgroundPageRoutingModule } from './full-screen-background-routing.module';

import { FullScreenBackgroundPage } from './full-screen-background.page';
import { CustomComponentsModule } from 'src/app/components/custom-components.module';

@NgModule({
  imports: [
    //components
    CustomComponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    FullScreenBackgroundPageRoutingModule
  ],
  declarations: [FullScreenBackgroundPage]
})
export class FullScreenBackgroundPageModule {}
