import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home-page/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'components',
    loadChildren: () => import('./pages/components-page/components.module').then( m => m.ComponentsPageModule)
  },
  {
    path: 'storage',
    loadChildren: () => import('./pages/storage-page/storage.module').then( m => m.StoragePageModule)
  },
  {
    path: 'full-screen-background',
    loadChildren: () => import('./pages/full-screen-background/full-screen-background.module').then( m => m.FullScreenBackgroundPageModule)
  },
  {
    path: 'videoplayer',
    loadChildren: () => import('./pages/videoplayer/videoplayer.module').then( m => m.VideoplayerPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
