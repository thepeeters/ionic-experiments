import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
//import { Toast } from '@capacitor/toast';


import { Capacitor } from '@capacitor/core';
import { CapacitorVideoPlayer } from 'capacitor-video-player';
export const setVideoPlayer = async (): Promise<any>=> {
  const platform = Capacitor.getPlatform();
  console.log(`platform ${platform}`);
  return {plugin:CapacitorVideoPlayer, platform};
};



const videoFrom = 'http';

@Component({
  selector: 'app-fullscreen',
  templateUrl: './fullscreen.page.html',
  styleUrls: ['./fullscreen.page.scss'],
})
export class FullscreenPage implements OnInit {
  @Input() url: string;

  @Input() platform: string;

  private videoPlayer: any;
  private handlerExit: any;
  private mUrl: string = null;
  private mRate = 1.0;

  constructor(public modalCtrl: ModalController) { }

  async ngOnInit() {
    console.log('in ngOnInit');
    const player: any = await setVideoPlayer();
    this.videoPlayer = player.plugin;
    await this.addListenersToPlayerPlugin();
  }
  async ionViewDidEnter() {
    this.mUrl = this.url;
  
  
    this.mRate = 1.0;
    if (this.mUrl != null) {
      const res: any  = await this.videoPlayer.initPlayer({mode: 'fullscreen',url: this.mUrl,
                                                          playerId: 'fullscreen', rate: this.mRate, exitOnEnd: true,
                                                          loopOnEnd: false, componentTag:'app-fullscreen'});                                                    
      console.log(`res ${JSON.stringify(res)}`);
      if(!res.result && (this.platform === 'ios' || this.platform === 'android')) {
        console.log({
          text: res.message,
          duration: 'long',
          position: 'bottom'
        });
      }
      console.log('res.result ',res.result) ;
      if (!res.result) {console.log(`res.message ${res.message}`);}
    }
  }
  public async closeModal(){
    this.leaveModal();
  }

  private async leaveModal(): Promise<void> {
    console.log('$$$$$ in leaveModal $$$$');
    await this.videoPlayer.stopAllPlayers();
    console.log('$$$$$ in leaveModal after stopAllPlayers $$$$');
    this.handlerExit.remove();
    // Dismiss the modal view
    this.modalCtrl.dismiss({
      dismissed: true
    });
    return;
  }
  private async addListenersToPlayerPlugin(): Promise<void> {
    this.handlerExit = await this.videoPlayer.addListener('jeepCapVideoPlayerExit', (data: any) => this.playerExit(data), false);
    return;
  }

  private async playerExit(data: any): Promise<void> {
      console.log(`Event jeepCapVideoPlayerExit ${data}`);
      await this.leaveModal();
      return;
  }


}
