import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.example.experiments',
  appName: 'ionic-experiments',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
