import { CapacitorProject } from '@capacitor/project';
import { CapacitorConfig } from '@capacitor/cli';

// This takes a CapacitorConfig, such as the one in capacitor.config.ts, but only needs a few properties
// to know where the ios and android projects are
const config: CapacitorConfig = {
  /*ios: {
    path: 'ios',
  },*/
  android: {
    path: 'android',
  },
};

const updateProject = async () => {
  const project = new CapacitorProject(config);
  await project.load();
  //console.log(project)

  //project.android.getPackageName() 
  // sempre retorna io.ionic.starter


  await project.android?.setPackageName('com.experiments.ionic'); 
  //Un cop canviat el nom del package comenta aquesta linia ja que provoca excepció

  await project.android?.setVersionName('1.42.1')
  await project.android?.setVersionCode(42)

  await project.android?.addResource('raw', 'test.json', '{myalue:test}')
  await project.android?.copyToResources('drawable', 'test.png', './configure/test-image.png')

  //await project.android?.addResource('app', 'google-services.json', './configure/google-services-app.json')

  project.vfs.commitAll()
}

updateProject()


/*
tsconfig.json
"compilerOptions": {
   ....
    "target": "esnext",
    "module": "esnext",
....
  },


RUN npx tsc configure/configure.ts
RUN npx tsc configure/configure.ts && node configure/configure.js
*/